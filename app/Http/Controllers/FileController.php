<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    public function show()
    {
        $content = Storage::get('file.jpg');
        $url = Storage::url('file.jpg'); 
        return view('profile', [
            'test' => $url,
            'file' => $content
        ]);

    }
}
