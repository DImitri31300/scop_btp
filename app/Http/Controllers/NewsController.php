<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class NewsController extends Controller
{
    public function index()
    {
         $news = DB::table('news')->get();
        return view('news', [
                    'news' => $news]);
    }
}
