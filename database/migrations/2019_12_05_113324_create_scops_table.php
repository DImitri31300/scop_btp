<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateScopsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scops', function(Blueprint $table)
		{
			$table->bigInteger('id', true)->unsigned();
			$table->timestamps();
			$table->string('Nom_scop', 45);
			$table->string('Activites', 45)->nullable();
			$table->integer('Departement');
			$table->string('Mail', 45)->nullable();
			$table->string('Telephone', 45)->nullable();
			$table->string('Site_internet', 60)->nullable();
			$table->string('Engagee', 45);
			$table->string('Labellisee', 45);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('scops');
	}

}
