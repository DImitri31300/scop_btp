@extends('layouts.app')


@section('content')
 {{-- ________________________________TITRE_______________________________________________ --}}
 <div class="jumbotron jumbotron-fluid bg-white" id="title_annuaire">      
    <div class="title_titre">
      <h1 class="display-4 text-center"> Annuaire des SCOPS adhérentes</h1>
    </div>
</div>


@foreach($scops as $scop)

<div class="card-deck">
        <div class="card bg-light mb-3" style="max-width: 18rem;">
            <div class="card-header">{{ $scop->Nom_scop }}</div>
            <div class="card-body">
                <p class="card-text">Activité(s) de la Scop : {{ $scop->Activites }}</p>
                <p class="card-text">Département : {{ $scop->Departement }}</p>
                <p class="card-text">Mail : {{ $scop->Mail }}</p>
                <p class="card-text">Téléphone : {{ $scop->Telephone }}</p>
                <p class="card-text">Site Internet : {{ $scop->Site_internet }}</p>
                <p class="card-text">Engagée dans une démarche RSE : {{ $scop->Engagee }}</p>
                <p class="card-text">Labellisée RSE : {{ $scop->Labellisee }}</p>

                </div>
            </div>
        </div>
    </div>
    @endforeach
    @endsection

