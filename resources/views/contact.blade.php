@extends('layouts.app')

@section('content')
{{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_contact">      
    <div class="title_titre">
      <h1 class="display-4 text-center">Contact</h1>
    </div>
</div>
<!-- Bloc Contact  -->
<br>
<section class="container text-center bg-light" id="block-engaged"><br><br>
    <h3 style="color:#588623">Fédération Sud-Ouest Des Scop du BTP </h3><br>
    <div class="bloc_engager mx-auto">
      <img src="img/logo.jpg" alt="" width="300px" height="300px">
      <div class="text_engager">
        <ul> 
          <li><ion-icon name="home"></ion-icon>Parc du Canal, Napa Center Bât A <br>
            <span style="margin:15px;">  </span>3 rue Ariane 31520 Ramonville st Agne
            </li> <br>
          <li><ion-icon name="call"></ion-icon> 05 61 00 20 18</li><br>
          <li><ion-icon name="at"></ion-icon><a href="mailto:sudouest@scopbtp.org">sudouest@scopbtp.org</a></li>  
        </ul>
      </div><br><br>
    </div> <br><br>
</section><br><br>
<hr style="border:1px solid lightgrey; width:90%"><br>
<!-- Bloc Map -->
<div class="container-fluid text-center mx-auto">
    <div class="map-responsive">
        <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2888.6799930921115!2d1.4544360504998213!3d43.6132041629326!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12aebcbc6d0c7c27%3A0x27b36675eda83ee!2sFederation%20Sud-Ouest%20Des%20Scop%20Du%20BTP!5e0!3m2!1sfr!2sfr!4v1575535742628!5m2!1sfr!2sfr"
        width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</div><br>
<hr style="border:1px solid lightgrey; width:90%"><br>
{{-- ------------> reseaux sociaux --}}
<section class="RS" id="ContainerRS">
    <div class="container-fluid text-center bg-light" >
        <div class=" w-75  mx-auto" > 
            <div class="bloc_RSActu"><br>
                <h4>Restons en contact sur les réseaux sociaux</h4><br>
                <div class="rs">
                    <a href="https://twitter.com/so_btp"><ion-icon class="logo_rs" name="logo-twitter"></ion-icon></a>
                    <a href="https://www.linkedin.com/company/f%C3%A9d%C3%A9ration-sud-ouest-scop-btp/?viewAsMember=true"><ion-icon class="logo_rs" name="logo-linkedin"></ion-icon></a>
                    <a href="https://www.youtube.com/user/SCOPBTP"><ion-icon class="logo_rs" name="logo-youtube"></ion-icon></a>    
                </div>
            </div>            
        </div>
    </div>
</section><br><br>
@endsection