@extends('layouts.app')

@section('content')
{{-- _______________________________BLOC TITRE_________________________________ --}}
      <div class=" jumbotron-fluid ">
            <div class="container">
              <h1 class="display-4 text-center" style="color:#588623">Bienvenue sur le site </h1>
              <h1 class="display-4 text-center" style="color:#588623" >de la Fédération SCOP BTP Sud-ouest</h1>
              <p class="lead text-center">Responsabilité, effort et confiance, le meilleur est devant nous.</p>
            </div>
     </div>
{{-- _______________________________CARROUSEL IMAGES_________________________________ --}}
<div id="carouselExampleCaptions" class="carousel slide "  data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
       
        <div class="carousel-inner" style="height: 500px;">
          <div class="carousel-item active">
            <img src="/img/caroussel_1.jpg" class="d-block w-100 " alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>First slide label</h5>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/img/caroussel_2.jpg" class="d-block w-100  "  alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Second slide label</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="/img/caroussel_3.jpg" class="d-block w-100 " alt="...">
            <div class="carousel-caption d-none d-md-block">
              <h5>Third slide label</h5>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
<section>
  
</section>

      
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
