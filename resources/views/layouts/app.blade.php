<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    

    
    <link href="css/app.css" rel="stylesheet">
    
    <title>SCOP BTP</title>
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-light "style="background-color: white;">
        <a class="navbar-brand" href="/home">
          <img src="/img/logo.jpg" width="150" height="150" alt="retour à l'accueil">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto ">
            <li class="nav-item active">
              <a class="nav-link" href="/home">Accueil <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/federation">La Fédération</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/rse">La RSE</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/news">L'Actualité</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/annuaire">L'Annuaire</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/agenda">L'Agenda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/contact">Contact</a>
            </li>
          </ul>
          @guest
          <form class="form-inline my-2 my-lg-0">
            <a class="nav-link btn btn-outline-success my-2 my-sm-0" href="/login">ESPACE ADHÉRENT</a>
          </form>
          
              @if (Route::has('register'))
           
                  @endif
              @else
              <li class="nav-item dropdown" style="list-style:none;">
                  <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                      <a class="dropdown-item"  href="{{ route('logout') }}"
                         onclick="event.preventDefault();
                                       document.getElementById('logout-form').submit();">
                          {{ __('Logout') }}
                      </a>
                      <a class="dropdown-item"  href="/profile/{id}">Mon espace</a>
                      
  
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                          @csrf
                      </form>
                  </div>
              </li>
              @endguest
        </div>
      </nav>
      <main class="py-4">    
      </main>
      {{-- {!! menu('main') !!} --}}
                  
  </header>

  @yield('content')

    <!-- Footer -->
<footer class="page-footer  font-small  pt-4 ">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col-md-6 mt-md-0 mt-3">
        <!-- Content -->
        <h5 class="text-uppercase">Fédération SCOP BTP Sud-Ouest</h5>
        <img src="/img/logo.jpg" width="200" height="200" alt="">
        <div class="rs_footer ">
            <a href="https://twitter.com/so_btp"><ion-icon  name="logo-twitter"></ion-icon></a>
            <a href="https://www.linkedin.com/company/f%C3%A9d%C3%A9ration-sud-ouest-scop-btp/?viewAsMember=true"><ion-icon  name="logo-linkedin"></ion-icon></a>
            <a href="https://www.youtube.com/user/SCOPBTP"><ion-icon  name="logo-youtube"></ion-icon></a>    
        </div>
      </div>
      <!-- Grid column -->
      <hr class="clearfix w-100 d-md-none pb-3">
      <div class="col-md-3 mb-md-0 mb-3">
        <!-- menu -->
        <h5 class="text-uppercase">Menu</h5>

        <ul class="list-unstyled">
          <li>
            <a href="/home">Accueil</a>
          </li>
          <li>
            <a href="/federation">La fédération</a>
          </li>
          <li>
            <a href="/rse">La RSE</a>
          </li>
          <li>
            <a href="/news">Actualité</a>
          </li>
          <li>
            <a href="/annuaire">Annuaire</a>
          </li>
          <li>
            <a href="/agenda">Agenda</a>
          </li>
          <li>
            <a href="#!">Espace adhérents</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3 mb-md-0 mb-3">
        <h5 class="text-uppercase">Contact</h5>

        <ul class="list-unstyled">
          <li>
            <p><strong>Adresse :</strong></p>
            <p>Parc du Canal, Napa Center
              Bâtiment A,
              3 rue Ariane
              31520 Ramonville Saint-Agne</p>
          </li>
          <li>
            <p><strong>Téléphone :</strong></p>
            <p>05 61 00 20 18</p>
          </li>
          <li>
              <p><strong>Email :</strong></p>
            <p>sudouest@scopbtp.org</p>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- Copyright -->
  <div  class="footer-copyright text-center py-3">
    <a style="font-size:15px" href="/mentions-legales">© 2019 Fédération SCOP BTP SO</a>
  </div>
</footer>
<script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
</body>

</html>
