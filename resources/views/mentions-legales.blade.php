@extends('layouts.app')


@section('content')

<h3> Mentions légales</h3>

<p>En vertu de l'article 6 de la loi n° 2004-575 du 21 juin 2004 pour la confiance dans l'économie numérique, il est précisé aux utilisateurs du site de la Fédération des SCOP du BTP (www.scopbtp.org) l'identité des différents intervenants dans le cadre de sa réalisation et de son suivi:
Propriétaire : Fédération des SCOP du BTP du Sud Ouest– Organisation à but non lucratif – 64 bis rue de Monceau, 75008 PARIS

Création : Les !mposteurs de Simpon - Caroline Chatelon, Cécile Couderc & Dimitri Klopfstein

Directeur de la publication : Monsieur Patrick ROUAIX

Hébergeur : ovh.com – 2 rue Kellermann - 59100 Roubaix - France.

Président :
M. Patrick ROUAIX

Secrétaire Générale :
Mme Véronique MALECKI</p>
@endsection