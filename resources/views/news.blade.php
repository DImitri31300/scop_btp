@extends('layouts.app')


@section('content')
 {{-- ________________________________TITRE_______________________________________________ --}}
 <div class="jumbotron jumbotron-fluid bg-white" id="title_new">      
        <div class="title_titre">
          <h1 class="display-4 text-center">ACTUALITÉ </h1>
         </div>
       </div>
{{-- {-- _________________________________ARTICLES__________________________________________ --}} 
<div class="container-fluid mx-auto text-center" >
        <br><h3 style="color:#949393">Retrouvez toute l'actualité de la Fédération et des SCOPS adhérentes</h3><br>
    {{-- _______________________________DERNIERS ARTICLES____________________________________ --}}
    
    <div class="box_news">
        <div class="last_news bg-light">
            <div>
                <img src="https://picsum.photos/id/492/200/200" witdh="200px" heigth="200px" alt="">
            </div>
                <div class="info_news">
                    <h5>Article demo </h5><br>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                       sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                       sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p><br>
                    <a href=""><button type="button" class="btn btn-outline-success">Lire la suite</button></a>
                </div>
        </div>
    </div>
<section class="list_article">
@foreach($news as $new)
<div id="ACTU">
<div class="card" style="width: 18rem;">
    <img src="{{ $new->image }}" class="card-img-top" alt="image représentant l'évènement">
    <div class="card-body">
        <h5 class="card-title">{{ $new->title }}</h5>
        <p class="card-text">{{ $new->excerpt }}</p>
        <button type="button" class="btn btn-outline-success">Lire la suite</button>
    </div>
</div>
</div><br>
@endforeach
</section>
</div><br><br>
{{-- ______________________________________RESEAUX SOCIAUX____________________________________________ --}}
<section class="RS" id="ContainerRS">
    <div class="container-fluid text-center bg-light" >
        <div class=" w-75  mx-auto" > 
            <div class="bloc_RSActu"><br>
                <h4>Suivez toute notre actualité sur les réseaux sociaux</h4><br>
                <div class="rs">
                    <a href="https://twitter.com/so_btp"><ion-icon class="logo_rs" name="logo-twitter"></ion-icon></a>
                    <a href="https://www.linkedin.com/company/f%C3%A9d%C3%A9ration-sud-ouest-scop-btp/?viewAsMember=true"><ion-icon class="logo_rs" name="logo-linkedin"></ion-icon></a>
                    <a href="https://www.youtube.com/user/SCOPBTP"><ion-icon class="logo_rs" name="logo-youtube"></ion-icon></a>    
                </div>
            </div>            
        </div>
    </div>
</section>
@endsection