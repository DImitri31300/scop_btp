@extends('layouts.app')

@section('content')
{{-- ________________________________TITRE_______________________________________________ --}}
<div class="jumbotron jumbotron-fluid bg-white" id="title_profil">      
    <div class="title_titre">
      <h1 class="display-4 text-center"> Espace scop Adhérent</h1>
    </div>
</div>
{{-- ---------> card profil adhérent --}}
<div class="container ">
    <h1 class="display-4">Nom : {{ Auth::user()->name }}</h1>
    <p class="lead text-muted">Inscrit Depuis le : {{ Auth::user()->created_at }}</p>
</div>
{{-- --------> formulaire envoie doccument  --}}
<section>
    <br><br>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header font-weight-bold">Envoyer un documents</div>
                        <div class="card-body">
                            <form action="{{ route('upload') }}" method="POST" role="form" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="file" class="col-md-4 col-form-label text-md-right">Télécherger un fichier :</label>
                                <div class="col-md-6">
                                    <input id="file" type="file" class="form-control @error('file') is-invalid @enderror" name="files[]" value="{{ old('file') }}" multiple>
                                    @error('file')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-success">Envoyer le fichier</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<br><br><br><br>
@endsection