<?php

use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
|
*/
//Accueil 
Route::view('/','home');
//RSE
Route::view('rse', 'rse');

// _____________________________-> Add route 'Home' for test 
Route::view('/home','home');

//Fédération 
Route::get('federation', 'GeneralController@federation');
// Route::get('/home', 'HomeController@index')->name('home');

//RSE
Route::view('rse', 'rse');

//Annuaire 
Route::get('/annuaire', 'AnnuaireScopsController@index');

// Contact
Route::view('/contact','contact');

//Mentions légales 
Route::view('/mentions-legales', 'mentions-legales' );


//Route des fichiers
Route::get('file', 'FileController@show');
Route::post('/upload', 'HomeController@upload')->name('upload');

//Actualité - NEWS
Route::get('news', 'NewsController@index');


//Agenda
Route::get('agenda', 'EventController@index');

//---Route pour la gestion---\\

// // Profil SCOP
Route::get('profile/{id}', 'UserController@show', 'FileController@show')->middleware('auth');

// Route admin voyager 

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
//Authentification
Auth::routes();

//Route des fichiers
Route::get('file', 'FileController@show');

// //Route des dates 
// Route::get('/time', function () {
//     $dt = new Carbon();
//     $dt->timezone('CET');
//     echo $dt;
// });






